const { app, BrowserWindow } = require("electron");
const path = require("path");
const url = require("url");
const updater = require('./updater')

let win;

function setAppUserModelId() {
  var updateDotExe = path.join(path.dirname(process.execPath), '..', 'update.exe');

  var packageDir = path.dirname(path.resolve(updateDotExe));
  var packageName = path.basename(packageDir);
  var exeName = path.basename(process.execPath).replace(/\.exe$/i, '');

  global.appUserModelId = `com.scrollnetwork.slidedrive.${exeName}`;
  app.setAppUserModelId(global.appUserModelId);
}

function createWindow() {
  win = new BrowserWindow({ width: 800, height: 600 });

  // load the dist folder from Angular
  win.loadURL(
    url.format({
      pathname: path.join(__dirname, '/dist/slid/index.html'),
      protocol: "file:",
      slashes: true
    })
  );

  // The following is optional and will open the DevTools:
  // win.webContents.openDevTools()

  win.on("closed", () => {
    win = null;
  });
}

app.on("ready", () => {
  createWindow();
  setTimeout(updater.check, 2000);
});

// on macOS, closing the window doesn't quit the app
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// initialize the app's main window
app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});